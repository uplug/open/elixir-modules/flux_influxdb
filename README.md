# Flux InfluxDB

[![pipeline
status](https://gitlab.com/uplug/open/elixir-modules/flux_influxdb/badges/master/pipeline.svg)](https://gitlab.com/uplug/open/elixir-modules/flux_influxdb/commits/master)
[![coverage
report](https://gitlab.com/uplug/open/elixir-modules/flux_influxdb/badges/master/coverage.svg)](https://gitlab.com/uplug/open/elixir-modules/flux_influxdb/commits/master)

Exception module to improve error management.

## Usage

Add [Flux InfluxDB](https://hex.pm/packages/flux_influxdb) as a dependency in
your `mix.exs` file:

```elixir
def deps do
  [{:flux_influxdb, "~> 0.0.1"}]
end
```

`FluxInfluxDB` describes how to manage
[InfluxDB](https://docs.influxdata.com/influxdb/v1.7/) data.

For more information about the query map structure, check [Query](query) page.

## Application Configuration

```elixir
import Config

# Default values
config :flux_influxdb,
  url: "http://influxdb:8086"
```

### Configuration Options

- `:url` - The InfluxDB URL.
