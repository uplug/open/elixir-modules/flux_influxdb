defmodule FluxInfluxDB.DocTest do
  use ExUnit.Case

  @moduletag :capture_log

  setup do
    FluxInfluxDB.drop_database!("my_database")
    :ok
  end

  doctest FluxInfluxDB
end
