defmodule FluxInfluxDBTest do
  use ExUnit.Case

  import FluxInfluxDB

  @database "test"
  @measurement "test"

  setup do
    drop_database!(@database)
    :ok
  end

  describe "delete!/4" do
    test "delete data based on query" do
      data_one = %{"test" => true}
      data_two = %{"test" => false}

      assert :ok == write!(data_one, [], @measurement, @database)
      assert :ok == write!(data_two, [], @measurement, @database)
      assert [point_one, point_two] = read!(%{}, @measurement, @database)
      assert :ok == delete!(%{"where" => %{"time" => point_one["time"]}}, @measurement, @database)
      assert [point_two] = read!(%{}, @measurement, @database)
    end
  end
end
