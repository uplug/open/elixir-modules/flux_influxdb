defmodule FluxInfluxDB.ReadParserTest do
  use ExUnit.Case, async: true

  import FluxInfluxDB.ReadParser, only: [to_influx_query: 2]

  @measurement "test"

  describe "to_influx_query/2" do
    test "generate query with fields if set" do
      query_map = %{"fields" => ~w(field_a field_b field_c)}
      result = ~s(SELECT field_a,field_b,field_c FROM "test" ORDER BY time DESC)

      assert result == to_influx_query(query_map, @measurement)
    end

    test "generate query with limit if set" do
      query_map = %{"limit" => 10}
      result = ~s(SELECT * FROM "test" ORDER BY time DESC LIMIT 10)

      assert result == to_influx_query(query_map, @measurement)
    end

    test "generate query with offset if set" do
      query_map = %{"offset" => 10}
      result = ~s(SELECT * FROM "test" ORDER BY time DESC OFFSET 10)

      assert result == to_influx_query(query_map, @measurement)
    end

    test "generate query with from if set" do
      query_map = %{"from" => "now() - 5d"}
      result = ~s(SELECT * FROM "test" WHERE time >= now\(\) - 5d ORDER BY time DESC)

      assert result == to_influx_query(query_map, @measurement)
    end

    test "generate query with to if set" do
      query_map = %{"to" => "now()"}
      result = ~s(SELECT * FROM "test" WHERE time <= now\(\) ORDER BY time DESC)

      assert result == to_influx_query(query_map, @measurement)
    end

    test "generate query with filter with multiple values" do
      query_map = %{"where" => %{"field" => ["hello", "world"]}}

      result =
        ~s(SELECT * FROM "test" WHERE "field" = 'hello' OR "field" = 'world' ORDER BY time DESC)

      assert result == to_influx_query(query_map, @measurement)
    end

    test "generate query with custom nested filter" do
      query_map = %{
        "where" => %{
          "all_and" => %{
            "field" => "hello"
          }
        }
      }

      result = ~s(SELECT * FROM "test" WHERE "field" = 'hello' ORDER BY time DESC)

      assert result == to_influx_query(query_map, @measurement)
    end

    test "generate query with custom symbol filter" do
      query_map = %{
        "where" => [
          %{
            "field_a" => %{"c" => "=~", "v" => "hello"}
          },
          %{
            "field_b" => %{"c" => "!=", "v" => 10}
          }
        ]
      }

      result =
        ~s(SELECT * FROM "test" WHERE "field_a" =~ /hello/ OR "field_b" != 10 ORDER BY time DESC)

      assert result == to_influx_query(query_map, @measurement)
    end
  end
end
