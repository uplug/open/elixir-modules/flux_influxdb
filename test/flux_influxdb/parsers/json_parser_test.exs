defmodule FluxInfluxDB.JSONParserTest do
  use ExUnit.Case, async: true

  import FluxInfluxDB.JSONParser, only: [decode!: 1]

  describe "decode!/1" do
    test "raise if failed to convert invalid JSON string to Elixir structure" do
      invalid_json = "i am not json"
      error = assert_raise FluxError, fn -> decode!(invalid_json) end
      assert error.reason == :payload_not_json
    end
  end
end
