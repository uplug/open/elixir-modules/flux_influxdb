defmodule FluxInfluxDB.WriteParserTest do
  use ExUnit.Case, async: true

  import FluxInfluxDB.WriteParser, only: [to_inline_data: 3]

  @measurement "test"

  describe "to_inline_data/3" do
    test "generate inline data with tags" do
      payload = %{"hello" => "world", "lorem" => "ipsum"}
      result = ~s(test,hello=world lorem=\"ipsum\")
      assert result == to_inline_data(payload, ["hello"], @measurement)
    end

    test "generate inline data with float or integer values" do
      payload = %{"hello" => 3.14, "world" => 42}
      result = ~s(test hello=3.14,world=42i)
      assert result == to_inline_data(payload, [], @measurement)
    end
  end
end
