defmodule FluxInfluxDB.ResultParserTest do
  use ExUnit.Case, async: true

  import FluxInfluxDB.ResultParser, only: [to_key_value!: 1]

  describe "to_key_value!/1" do
    test "return the series itself if without 'columns' and 'values'" do
      data = %{"results" => [%{"series" => ["Hello World"]}]}
      assert ["Hello World"] == to_key_value!(data)
    end

    test "return empty list if error message contains 'database not found'" do
      data = %{"results" => [%{"error" => "database not found"}]}
      assert [] == to_key_value!(data)
    end

    test "raise error on results as error" do
      data = %{"results" => [%{"error" => "InfluxDB failed"}]}
      error = assert_raise FluxError, fn -> to_key_value!(data) end
      assert :operation_failed == error.reason
    end

    test "raise error on error" do
      data = %{"error" => "InfluxDB failed"}
      error = assert_raise FluxError, fn -> to_key_value!(data) end
      assert :operation_failed == error.reason
    end

    test "raise error on unknown data structure" do
      data = "InfluxDB failed"
      error = assert_raise FluxError, fn -> to_key_value!(data) end
      assert :unknown_response == error.reason
    end
  end
end
