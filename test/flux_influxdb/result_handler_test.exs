defmodule FluxInfluxDB.ResultHandlerTest do
  use ExUnit.Case, async: true

  import FluxInfluxDB.ResultHandler, only: [handle!: 2]

  describe "handle!/4" do
    test "raise error on request error" do
      result = {:error, :failed}
      error_reason = :request_failed

      assert_raise FluxError, fn -> handle!(result, error_reason) end
    end

    test "raise error on status divergence" do
      result = {:ok, %{body: nil, status: 400}}
      error_reason = :invalid_status

      assert_raise FluxError, fn -> handle!(result, error_reason) end
    end
  end
end
