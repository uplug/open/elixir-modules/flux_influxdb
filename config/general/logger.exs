import Config

config :logger,
  backends: [:console],
  level: :debug,
  truncate: :infinity,
  discard_threshold: 5_000

config :logger, :console,
  format: "[$level$levelpad] $message $metadata\n",
  metadata: [
    :error_metadata,
    :error_stacktrace
  ]
