import Config

config :flux_error,
  messages_module: FluxInfluxDB.ErrorMessages,
  default_options: []
