defmodule FluxInfluxDB do
  @moduledoc """
  Manage InfluxDB data.
  """
  @moduledoc since: "0.0.1"

  alias FluxInfluxDB.{JSONParser, Reader, ReadParser, ResultParser, Sweeper, WriteParser, Writer}

  @doc """
  Delete
  [points](https://docs.influxdata.com/influxdb/v1.7/concepts/glossary/#point)
  based on [query](query).

  ## Parameters

    - `query_map` - A [query](query) `t:map/0`. Only accepts
      [time](https://docs.influxdata.com/influxdb/v1.7/concepts/glossary/#timestamp)
      filtering.

    - `measurement` - The database's
      [measurement](https://docs.influxdata.com/influxdb/v1.7/concepts/glossary/#measurement).

    - `database` - The
      [database](https://docs.influxdata.com/influxdb/v1.7/concepts/glossary/#database)
      name.

    - `options` - The InfluxDB
      [API](https://docs.influxdata.com/influxdb/v1.7/tools/api/) query options.

  ## Examples

      iex> FluxInfluxDB.delete!(%{}, "my_measurement", "my_database")
      :ok
  """
  @doc since: "0.0.1"
  @spec delete!(map, String.t(), String.t(), keyword) :: :ok
  def delete!(query_map, measurement, database, options \\ []) do
    query_map
    |> Map.put("order_asc", true)
    |> ReadParser.to_influx_query(measurement)
    |> Sweeper.delete!(database, options)
  end

  @doc """
  Remove
  [database](https://docs.influxdata.com/influxdb/v1.7/concepts/glossary/#database)
  and its data.

  ## Parameters

    - `database` - The
      [database](https://docs.influxdata.com/influxdb/v1.7/concepts/glossary/#database)
      name.

  ## Examples

      iex> FluxInfluxDB.drop_database!("my_database")
      :ok
  """
  @doc since: "0.0.1"
  @spec drop_database!(String.t()) :: :ok
  def drop_database!(database) do
    Sweeper.drop_database!(database)
  end

  @doc """
  Search for
  [points](https://docs.influxdata.com/influxdb/v1.7/concepts/glossary/#point)
  based on [query](query).

  ## Parameters

    - `query_map` - A [query](query) `t:map/0`.

    - `measurement` - The database's
      [measurement](https://docs.influxdata.com/influxdb/v1.7/concepts/glossary/#measurement).

    - `database` - The
      [database](https://docs.influxdata.com/influxdb/v1.7/concepts/glossary/#database)
      name.

    - `options` - The InfluxDB
      [API](https://docs.influxdata.com/influxdb/v1.7/tools/api/) query options.

  ## Examples

      iex> FluxInfluxDB.write!(%{"hello" => "world"}, [], "my_measurement", "my_database")
      ...> FluxInfluxDB.write!(%{"hello" => "human"}, [], "my_measurement", "my_database")
      ...> query_map = %{"where" => %{"hello" => "world"}}
      ...> [%{"hello" => name}] = FluxInfluxDB.read!(query_map, "my_measurement", "my_database")
      ...> name
      "world"
  """
  @doc since: "0.0.1"
  @spec read!(map, String.t(), String.t(), keyword) :: list(map)
  def read!(query_map, measurement, database, options \\ []) do
    query_map
    |> ReadParser.to_influx_query(measurement)
    |> Reader.read!(database, options)
    |> JSONParser.decode!()
    |> ResultParser.to_key_value!()
    |> List.flatten()
  end

  @doc """
  Write a
  [point](https://docs.influxdata.com/influxdb/v1.7/concepts/glossary/#point).

  If
  [database](https://docs.influxdata.com/influxdb/v1.7/concepts/glossary/#database)
  does not exist, it will be created.

  ## Parameters

    - `payload` - A `t:map/0` with keys as `t:String.t/0`.

    - `tags_keys` - The point tags keys. Must be a `t:list/0` of `t:String.t/0`.

    - `measurement` - The database's
      [measurement](https://docs.influxdata.com/influxdb/v1.7/concepts/glossary/#measurement).

    - `database` - The
      [database](https://docs.influxdata.com/influxdb/v1.7/concepts/glossary/#database)
      name.

    - `options` - The InfluxDB
      [API](https://docs.influxdata.com/influxdb/v1.7/tools/api/) query options.

  ## Examples

      iex> FluxInfluxDB.write!(%{"hello" => "world"}, [], "my_measurement", "my_database")
      :ok
  """
  @doc since: "0.0.1"
  @spec write!(map, list(String.t()), String.t(), String.t(), keyword) :: :ok
  def write!(payload, tags_keys, measurement, database, options \\ []) do
    payload
    |> WriteParser.to_inline_data(tags_keys, measurement)
    |> Writer.write!(database, options)
  end
end
