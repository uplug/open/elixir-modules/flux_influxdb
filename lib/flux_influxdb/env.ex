defmodule FluxInfluxDB.Env do
  @moduledoc false

  @spec url :: String.t()
  def url, do: Application.get_env(:flux_influxdb, :url, "http://influxdb:8086")
end
