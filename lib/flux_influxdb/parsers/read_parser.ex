defmodule FluxInfluxDB.ReadParser do
  @moduledoc false

  @spec to_influx_query(map, String.t()) :: String.t()
  def to_influx_query(query_map, measurement) do
    [
      fetch_select(measurement, Map.get(query_map, "fields")),
      fetch_where(query_map),
      fetch_order_asc(Map.get(query_map, "order_asc", false)),
      fetch_limit(Map.get(query_map, "limit")),
      fetch_offset(Map.get(query_map, "offset"))
    ]
    |> Enum.reject(&(&1 == ""))
    |> Enum.join(" ")
  end

  defp fetch_select(measurement, fields) do
    measurement = ~s("#{measurement}")

    fields =
      if fields != nil and is_list(fields) and not Enum.empty?(fields) do
        Enum.join(fields, ",")
      else
        "*"
      end

    "SELECT #{fields} FROM #{measurement}"
  end

  defp fetch_where(query_map) do
    case do_fetch_where(query_map) do
      "" -> ""
      where -> "WHERE #{where}"
    end
  end

  defp fetch_order_asc(order_asc?) do
    if order_asc? == false do
      "ORDER BY time DESC"
    else
      ""
    end
  end

  defp fetch_limit(limit) do
    if limit do
      "LIMIT #{limit}"
    else
      ""
    end
  end

  defp fetch_offset(offset) do
    if offset do
      "OFFSET #{offset}"
    else
      ""
    end
  end

  defp do_fetch_where(params) do
    [
      fetch_where_conditions(Map.get(params, "where")),
      fetch_from_condition(Map.get(params, "from")),
      fetch_to_condition(Map.get(params, "to"))
    ]
    |> Enum.reject(&(&1 == ""))
    |> Enum.join(" AND ")
  end

  defp fetch_where_conditions(where) do
    cond do
      is_list(where) and not Enum.empty?(where) ->
        where
        |> Enum.map(&fetch_where_condition/1)
        |> Enum.reject(&(&1 == ""))
        |> Enum.join(" OR ")

      is_map(where) and not Enum.empty?(where) ->
        where
        |> Enum.map(&fetch_where_condition/1)
        |> Enum.reject(&(&1 == ""))
        |> Enum.join(" AND ")

      true ->
        ""
    end
  end

  defp fetch_from_condition(from) do
    if from do
      "time >= #{from}"
    else
      ""
    end
  end

  defp fetch_to_condition(to) do
    if to do
      "time <= #{to}"
    else
      ""
    end
  end

  defp fetch_where_condition(condition) do
    case condition do
      conditions when is_map(conditions) ->
        conditions
        |> Enum.map(&fetch_where_condition/1)
        |> Enum.reject(&(&1 == ""))
        |> Enum.join(" AND ")

      {key, values} when is_list(values) ->
        values
        |> Enum.map(&create_condition(key, &1))
        |> Enum.reject(&(&1 == ""))
        |> Enum.join(" OR ")

      {key, values} when is_map(values) ->
        fetch_map_condition(key, values)

      {key, value} ->
        create_condition(key, value)

      _condition ->
        ""
    end
  end

  defp fetch_map_condition(key, values) do
    condition_symbol = Map.get(values, "c")
    value = Map.get(values, "v")

    if values != nil and value != nil do
      create_condition(key, value, condition_symbol)
    else
      fetch_where_condition(values)
    end
  end

  defp create_condition(key, value, condition_symbol \\ "=") do
    case value do
      value when is_binary(value) ->
        if condition_symbol in ["=~", "!~"] do
          ~s("#{key}" #{condition_symbol} /#{value}/)
        else
          ~s("#{key}" #{condition_symbol} '#{value}')
        end

      value ->
        ~s("#{key}" #{condition_symbol} #{value})
    end
  end
end
