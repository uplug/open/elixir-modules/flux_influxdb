defmodule FluxInfluxDB.JSONParser do
  @moduledoc false

  @spec decode!(String.t()) :: list(map) | map
  def decode!(payload) do
    Jason.decode!(payload)
  rescue
    error -> reraise FluxError.from(error, :payload_not_json), __STACKTRACE__
  end
end
