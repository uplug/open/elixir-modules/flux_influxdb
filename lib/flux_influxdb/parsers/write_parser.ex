defmodule FluxInfluxDB.WriteParser do
  @moduledoc false

  @spec to_inline_data(map, list(String.t()), String.t()) :: String.t()
  def to_inline_data(payload, tags_keys, measurement) do
    {tags, fields, time} = tags_fields_and_time(payload, tags_keys)

    [
      measurement_and_tags(measurement, tags),
      fields,
      time
    ]
    |> Enum.reject(fn inline -> inline == "" end)
    |> Enum.join(" ")
  end

  defp tags_fields_and_time(data, tags_keys) do
    {tags, fields} = Map.split(data, tags_keys)
    {time, fields} = Map.pop(fields, "time", "")
    {inline_tags(tags), inline_fields(fields), time}
  end

  defp measurement_and_tags(measurement, tags) do
    if tags != "" do
      "#{measurement},#{tags}"
    else
      "#{measurement}"
    end
  end

  defp inline_tags(tags) do
    tags
    |> Enum.map(fn {key, value} -> "#{key}=#{value}" end)
    |> Enum.join(",")
  end

  defp inline_fields(fields) do
    fields
    |> Enum.map(&inline_field/1)
    |> Enum.reject(&(&1 == ""))
    |> Enum.join(",")
  end

  defp inline_field({key, value}) do
    case value do
      nil -> ""
      value when is_boolean(value) -> "#{key}=#{value}"
      value when is_float(value) -> "#{key}=#{value}"
      value when is_integer(value) -> "#{key}=#{value}i"
      value -> ~s(#{key}="#{value}")
    end
  end
end
