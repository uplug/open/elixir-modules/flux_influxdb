defmodule FluxInfluxDB.ResultParser do
  @moduledoc false

  @spec to_key_value!(map) :: list(map) | map
  def to_key_value!(%{"results" => [%{"error" => error}]}) do
    if error =~ "database not found" do
      []
    else
      raise FluxError.new(:operation_failed, detail: error)
    end
  end

  def to_key_value!(%{"error" => error}) do
    raise FluxError.new(:operation_failed, detail: error)
  end

  def to_key_value!(%{"results" => results}) when is_list(results) do
    Enum.map(results, &parse_result/1)
  end

  def to_key_value!(data) do
    raise FluxError.new(:unknown_response, data: data)
  end

  defp parse_result(%{"series" => [series]}) do
    parse_series(series)
  end

  defp parse_result(_result) do
    []
  end

  defp parse_series(%{"columns" => columns, "values" => values}) do
    Enum.map(values, &create_element(columns, &1))
  end

  defp parse_series(series) do
    series
  end

  defp create_element(keys, values) do
    keys
    |> Enum.zip(values)
    |> Enum.reduce(%{}, fn {key, value}, element -> Map.put(element, key, value) end)
  end
end
