defmodule FluxInfluxDB.Sweeper do
  @moduledoc false

  alias FluxInfluxDB.{Env, ResultHandler}

  @spec delete!(String.t(), String.t(), keyword) :: :ok
  def delete!(query, database, options \\ []) do
    url = Env.url() <> "/query"
    query = [db: database, q: String.replace(query, "SELECT *", "DELETE")] ++ options

    url
    |> Tesla.post("", query: query)
    |> ResultHandler.handle!(:delete_request_failed)
  end

  @spec drop_database!(String.t()) :: :ok
  def drop_database!(database) do
    url = Env.url() <> "/query"
    query = [q: ~s(DROP DATABASE "#{database}")]

    url
    |> Tesla.post("", query: query)
    |> ResultHandler.handle!(:drop_request_failed)
  end
end
