defmodule FluxInfluxDB.Writer do
  @moduledoc false

  alias FluxInfluxDB.{Env, ResultHandler}

  @spec write!(String.t(), String.t(), keyword) :: :ok
  def write!(inline_data, database, options \\ []) do
    url = Env.url() <> "/write"
    query = [db: database] ++ options

    url
    |> Tesla.post(inline_data, query: query)
    |> ResultHandler.handle!(:write_request_failed, :status, [200, 204, 404])
    |> maybe_create_database_and_try_again!(inline_data, database)
  end

  defp maybe_create_database_and_try_again!(status, inline_data, database) do
    if status == 404 do
      create_database!(database)
      write!(inline_data, database)
    else
      :ok
    end
  end

  defp create_database!(database) do
    url = Env.url() <> "/query"
    query = [q: ~s(CREATE DATABASE "#{database}")]

    url
    |> Tesla.post("", query: query)
    |> ResultHandler.handle!(:write_request_failed)
  end
end
