defmodule FluxInfluxDB.ResultHandler do
  @moduledoc false

  @spec handle!({:ok, map} | {:error, atom | map}, atom, atom, list(integer)) :: :ok | String.t()
  def handle!(result, error_reason, result_type \\ nil, valid_statuses \\ [200, 204]) do
    case result do
      {:ok, %{body: body, status: status}} ->
        handle_result!(body, status, valid_statuses, result_type, error_reason)

      {:error, error} ->
        invalid!(error, error_reason)
    end
  end

  defp handle_result!(body, status, valid_statuses, result_type, error_reason) do
    if status in valid_statuses do
      case result_type do
        :body -> body
        :status -> status
        _result_type -> :ok
      end
    else
      invalid!(body, status, error_reason)
    end
  end

  defp invalid!(error, reason) do
    raise FluxError.from(error, reason)
  end

  defp invalid!(body, status, reason) do
    raise FluxError.new(reason, body: body, status: status)
  end
end
