defmodule FluxInfluxDB.ErrorMessages do
  @moduledoc false

  use FluxError.Messages

  @impl FluxError.Messages
  def get_message(:delete_request_failed, _opts) do
    "failed to request deletion to database"
  end

  def get_message(:drop_request_failed, _opts) do
    "failed to request a drop to database"
  end

  def get_message(:failed_to_encode_to_json, _opts) do
    "the structure must be parseable to JSON"
  end

  def get_message(:operation_failed, _opts) do
    "InfluxDB failed to perform operation"
  end

  def get_message(:payload_not_json, _opts) do
    "payload must be a valid JSON"
  end

  def get_message(:read_request_failed, _opts) do
    "failed to request data to database"
  end

  def get_message(:unknown_response, _opts) do
    "unknown response from InfluxDB"
  end

  def get_message(:write_request_failed, _opts) do
    "failed to write data into database"
  end

  def get_message(_reason, _opts) do
    "triggered unhandled error"
  end
end
