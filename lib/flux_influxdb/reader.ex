defmodule FluxInfluxDB.Reader do
  @moduledoc false

  alias FluxInfluxDB.{Env, ResultHandler}

  @spec read!(String.t(), String.t(), keyword) :: String.t()
  def read!(query, database, options \\ []) do
    url = Env.url() <> "/query"
    query = [db: database, q: query] ++ options

    url
    |> Tesla.get(query: query)
    |> ResultHandler.handle!(:read_request_failed, :body)
  end
end
