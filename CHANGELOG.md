# Changelog

## 0.0.3 - 2020-02-29

- **Changed**:

  - `FluxInfluxDB.write!/5` now ignore keys with value as `nil`.

## 0.0.2 - 2020-02-29

- **Changed**:

  - InfluxDB url now can be set at runtime. See
    [mix release](https://hexdocs.pm/mix/Mix.Tasks.Release.html)

## 0.0.1 - 2020-02-19

- **Added**:

  - The project is available on GitLab and Hex.

- **Notes**:

  - Minor changes (0.0.x) from the current version will be logged to this file.

  - When a major change is released (0.x.0 or x.0.0), the changelog of the
    previous major change will be grouped as a single change.
