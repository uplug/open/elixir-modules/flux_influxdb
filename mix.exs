defmodule FluxInfluxDB.MixProject do
  use Mix.Project

  @version "0.0.3"
  @source_url "https://gitlab.com/uplug/open/elixir-modules/flux_influxdb"

  def project do
    [
      app: :flux_influxdb,
      name: "Flux InfluxDB",
      description: "Manage InfluxDB data",
      source_url: @source_url,
      version: @version,
      elixir: "~> 1.10",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      test_coverage: [tool: ExCoveralls],
      aliases: aliases(),
      deps: deps(),
      docs: docs(),
      package: package(),
      preferred_cli_env: preferred_cli_env()
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp aliases do
    [
      test: "test --no-start",
      "test.all": ["test.static", "test.coverage"],
      "test.coverage": ["coveralls"],
      "test.static": ["format --check-formatted", "credo list --strict --all"]
    ]
  end

  defp deps do
    [
      {:credo, "~> 1.1.3", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.21.2", only: [:dev, :test], runtime: false},
      {:excoveralls, "~> 0.12.2", only: [:dev, :test]},
      {:flux_error, "~> 0.0.1", runtime: false},
      {:hackney, "~> 1.15.2"},
      {:tesla, "~> 1.3.1"}
    ]
  end

  defp docs do
    [
      main: "readme",
      authors: ["Jonathan Moraes"],
      extras: ~w(CHANGELOG.md README.md docs/query.md)
    ]
  end

  defp package do
    [
      maintainers: ["Jonathan Moraes"],
      licenses: ["Apache-2.0"],
      files: ~w(docs lib mix.exs CHANGELOG.md LICENSE README.md),
      links: %{"GitLab" => @source_url}
    ]
  end

  defp preferred_cli_env do
    [
      coveralls: :test,
      "coveralls.detail": :test,
      "coveralls.post": :test,
      "coveralls.html": :test,
      "test.all": :test,
      "test.coverage": :test,
      "test.static": :test
    ]
  end
end
