# Query

## Query Key Definitions

Every key MUST be `t:String.t/0`. Do not use `t:atom/0`.

### `fields`

- **Type**: `t:list/0` of `t:String.t/0`
- **Default**: Empty `t:list/0`

If empty `t:list/0`, `FluxInfluxDB` will retrieve all fields.

Otherwise, `FluxInfluxDB` will return all fields described, even if it does not
exists (it will return with value `null`).

### `from`

- **Type**: [InfluxDB
  time](https://docs.influxdata.com/influxdb/v1.7/query_language/data_exploration/#time-syntax)
  (`t:String.t/0`)
- **Default**: Empty `t:String.t/0`

If empty `t:String.t/0`, `FluxInfluxDB` will not restrict the query to a lower
boundary of time.

Otherwise, `FluxInfluxDB` will restrict the query based on value received.

### `limit`

- **Type**: `t:integer/0`
- **Default**: `0`

If `0`, `FluxInfluxDB` will retrieve all data after
[offset](#data-query-offset).

Otherwise, `FluxInfluxDB` will retrieve `n` data after
[offset](#data-query-offset).

### `offset`

- **Type**: `t:integer/0`
- **Default**: `0`

If `0`, `FluxInfluxDB` will retrieve without any offset.

Otherwise, `FluxInfluxDB` will retrieve after an offset of size `n`.

### `order_asc`

- **Type**: `t:boolean/0`
- **Default**: `false`

If `true`, `FluxInfluxDB` will retrieve data with ascendent order (From oldest
to latest).

Otherwise, `FluxInfluxDB` will retrieve data with descendant order (From latest
to oldest).

### `to`

- **Type**: [InfluxDB
  time](https://docs.influxdata.com/influxdb/v1.7/query_language/data_exploration/#time-syntax)
  (`t:String.t/0`)
- **Default**: Empty `t:String.t/0`

If empty `t:String.t/0`, `FluxInfluxDB` will not restrict the query to a upper
boundary of time.

Otherwise, `FluxInfluxDB` will restrict the query based on value received.

### `where`

- **Type**: `t:list/0` of `t:map/0`, or `t:map/0`.

Restrict the query based on statements.

Using `t:list/0`, each element will be joined by an `OR` operator.

Using `t:map/0`, each element will be joined by an `AND` operator.

Each statement must contain the field name and the expected condition.

A condition can be a specific value, such as:

```elixir
%{"name" => "John Doe"}
```

A condition can be one of multiple values, such as:

```elixir
%{"age" => [18, 22, 30]}
```

A condition can also be composed [based on
InfluxDB](https://docs.influxdata.com/influxdb/v1.7/query_language/data_exploration/#the-where-clause)
by using a `t:map/0` with `c` and `v` keys. `c` is the condition and `v` is the
value.

## Examples

### Complete query with simple AND

```elixir
%{
  "fields" => ["name", "age", "phone"],
  "where" => %{
    "country" => "United States",
    "name" => %{"c" => "=~", "v" => "Alderson"},
    "company" => ["evil corp", "allsafe"]
  },
  "from" => "now() - 100d",
  "to" => "now()",
  "order_asc" => true,
  "offset" => 20,
  "limit" => 5
}
```

### Query with simple OR

```elixir
%{
  "where" => [
    %{"job" => "Jedi"},
    %{"name" => "Darth Vader"}
  ]
}
```
